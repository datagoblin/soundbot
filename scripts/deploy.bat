@echo off
echo deploying file "%~dp0..\zip\lambda.zip" to aws
aws lambda update-function-code --function-name trigger_sound_bot --zip-file fileb://%~dp0..\zip\lambda.zip
