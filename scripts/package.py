from glob import glob
from os import chdir, path
from zipfile import ZipFile, ZIP_DEFLATED

chdir(path.dirname(__file__))

source_files = [(file, file.replace("\\", "/").split("../")[-1]) for file in glob("../soundbot/**/*.py", recursive=True)]
binary_files = [(file, file.replace("\\", "/").split("../")[-1]) for file in glob("../soundbot/bin/*") if not file.endswith(".exe")]
config_files = [(file, file.replace("\\", "/").split("../")[-1]) for file in glob("../config/*.json")]
dependencies = [(file, file.replace("\\", "/").split("../env/Lib/site-packages/")[-1]) for file in glob("../env/Lib/site-packages/**", recursive=True)[1:]]

files_to_zip = source_files + \
               binary_files + \
               config_files + \
               dependencies + \
               [("../tmp", "tmp")]

with ZipFile("../zip/lambda.zip", "w") as lambda_zip:
    for file in files_to_zip:
        lambda_zip.write(file[0], file[1], compress_type = ZIP_DEFLATED)
    print("lambda package built: zip/lambda.zip")
