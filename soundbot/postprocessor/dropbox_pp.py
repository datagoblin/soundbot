import dropbox

from dropbox.files import WriteMode
from os.path import basename

from youtube_dl.postprocessor.common import PostProcessor

class DboxPP(PostProcessor):
    def __init__(self, downloader=None, token=None):
        self.dbx = dropbox.Dropbox(token)
        if downloader:
            downloader.to_screen("[dbox] token: " + token)
        super().__init__(downloader)

    def run(self, information):
        self.upload(information['filepath'])
        return super().run(information)

    def upload(self, filepath):
        if (self._downloader):
            self._downloader.to_screen("[dbox] uploading: " + filepath)
        with open(filepath, 'rb') as f:
            data = f.read()
            self.dbx.files_upload(
                data,
                "/.dropbot/_Unsorted/" + basename(filepath),
                WriteMode("overwrite")
            )
