import json
import os

from subprocess import check_output

from soundbot import downloader
from soundbot.telegram import Message, Messenger

def handle_event(event, context, config):
    update = json.loads(event["body"])
    raw_msg = update["message"]
    in_message = Message(raw_msg["chat"]["id"], raw_msg["text"])

    print("RECEIVED:", in_message.text)

    messenger = Messenger(config["telegram"])

    if (messenger.adm_chat_id != in_message.chat_id):
        messenger.send_message(
            "Hello! This bot is not yet available to you. "
            "Please check again later.",
            in_message.chat_id
        )
        return

    messenger.send_message("Parsing track: " + in_message.text)

    try:
        downloader.rip_audio(in_message.text, config["dropbox"], messenger)
        pass
    except downloader.InvalidAudioSourceError:
        message_text = "Oops, bad file"
    else:
        message_text = "Track succesfully downloaded!"
