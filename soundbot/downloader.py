from youtube_dl import YoutubeDL, postprocessor

from soundbot.postprocessor.dropbox_pp import DboxPP

def rip_audio(url, db_config, messenger):
    options = {
        "outtmpl": "/tmp/%(title)s.%(ext)s",
        "noplaylist": True,
        "postprocessors": [
            {
                "key": "FFmpegExtractAudio",
                "preferredcodec": "mp3"
            },
            {
                "key": "Dbox",
                "token": db_config["token"]
            }
        ]
    }

    # inject DropboxPP into post processors' namespace
    postprocessor.DboxPP = DboxPP

    with YoutubeDL(options) as downloader:
        downloader.download([url])
        messenger.send_message("Track succesfully downloaded!")

class InvalidAudioSourceError(Exception):
    pass
