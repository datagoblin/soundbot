import json
import os

from shutil import copyfile

from soundbot import lambda_handler

def test():
    with open("config/test_event.json", "rb") as test_event:
        event = {"body": test_event.read()}

    handle_lambda_event(event, None)

def handle_lambda_event(event, context):
    config = setup()

    lambda_handler.handle_event(event, context, config)

    return {
        "statusCode": 200,
        "headers": {"Content-Type": "application/json"},
        "body": "all is good"
    }

def setup():
    if os.name != "nt":
        copyfile("soundbot/bin/ffmpeg", "/tmp/ffmpeg")
        copyfile("soundbot/bin/ffprobe", "/tmp/ffprobe")

        os.chmod("/tmp/ffmpeg", 0o777)
        os.chmod("/tmp/ffprobe", 0o777)

        os.environ["PATH"] += os.pathsep + "/tmp"

    with open("config/config.json") as telegram_config:
        return json.load(telegram_config)

if __name__ == "__main__":
    test()
