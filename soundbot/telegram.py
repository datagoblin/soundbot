import os

from collections import namedtuple
from requests import post

TELEGRAM_URL = "https://api.telegram.org/bot{0}/sendMessage"

Message = namedtuple("Message", "chat_id text")

class Messenger:
    def __init__(self, config):
        self.bot_token = config["bot_token"]
        self.adm_chat_id = config["chat_id"]

    def send_message(self, text, chat_id=None):
        if chat_id is None:
            chat_id = self.adm_chat_id

        msg = Message(chat_id, text)

        post(TELEGRAM_URL.format(self.bot_token), data=msg._asdict())
